import 'package:flutter/material.dart';
import 'package:flutter_application_1/second_page_car.dart';

class rental extends StatefulWidget {
  const rental({super.key});

  State<rental> createState() => rentalState();
}

class rentalState extends State<rental> {
  int _selectedIndex = 0;

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });

    // Navigate to the selected screen
    switch (index) {
      case 1:
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(builder: (context) => CarScreen()),
        );
        break;
    }
  }

  Widget build(BuildContext context) {
    return Scaffold(
      // backgroundColor: Color.fromARGB(255, 243, 241, 237),
      appBar: AppBar(
        backgroundColor: Color.fromARGB(255, 220, 151, 48),
        title: const Text(
          "Bike4u",
          style: TextStyle(
              fontStyle: FontStyle.italic, fontWeight: FontWeight.bold),
        ),
        centerTitle: true,
        leading: IconButton(onPressed: () {}, icon: Icon(Icons.menu)),
        actions: [IconButton(onPressed: () {}, icon: Icon(Icons.search))],
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              child: Text(
                "Jupiter",
                style: TextStyle(
                    color: Colors.black,
                    fontStyle: FontStyle.italic,
                    //   fontWeight: FontWeight.bold,
                    fontSize: 25,
                    shadows: [
                      Shadow(color: Colors.black, offset: Offset(2, 1))
                    ]),
              ),
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: [
                  Container(
                    child: Column(children: [
                      Image.network(
                        "https://bd.gaadicdn.com/processedimages/tvs/jupiter-125/source/jupiter-1256528e732c44b5.jpg?impolicy=resize&imwidth=360",
                        height: 250,
                        width: 350,
                      ),
                      ElevatedButton(
                          onPressed: () {},
                          child: Text(
                            "ADD",
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.w600),
                          ))
                    ]),
                  ),
                  SizedBox(
                    width: 30,
                  ),
                  Container(
                    child: Column(children: [
                      Image.network(
                        "https://www.carandbike.com/_next/image?url=https%3A%2F%2Fimages.carandbike.com%2Fbike-images%2Fgallery%2Ftvs%2Fjupiter%2Fexterior%2F360-01.jpg%3Fv%3D2017-01-24&w=640&q=75",
                        height: 200,
                        width: 300,
                      ),
                      ElevatedButton(
                          onPressed: () {},
                          child: Text(
                            "ADD",
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.w600),
                          ))
                    ]),
                  ),
                  SizedBox(
                    width: 50,
                  ),
                  Container(
                    child: Column(children: [
                      Image.network(
                        "https://assets.otocapital.in/prod/starlight-blue-tvs-jupiter-image.jpeg",
                        height: 200,
                        width: 260,
                      ),
                      ElevatedButton(
                          onPressed: () {},
                          child: Text(
                            "ADD",
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.w600),
                          ))
                    ]),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              "Activa",
              style: TextStyle(
                  color: Colors.black,
                  fontStyle: FontStyle.italic,
                  //fontWeight: FontWeight.bold,
                  fontSize: 25,
                  shadows: [Shadow(color: Colors.black, offset: Offset(2, 1))]),
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: [
                  Container(
                    child: Column(children: [
                      Image.network(
                        "https://imgd.aeplcdn.com/1280x720/n/bw/models/colors/suzuki-select-model-metallic-matte-platinum-silver-no-2-1679635810668.png?q=100",
                        height: 200,
                        width: 400,
                      ),
                      ElevatedButton(
                          onPressed: () {},
                          child: Text(
                            "ADD",
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.w600),
                          ))
                    ]),
                  ),
                  SizedBox(
                    width: 30,
                  ),
                  Container(
                    child: Column(children: [
                      Image.network(
                        "https://imgd.aeplcdn.com/1056x594/n/bw/models/colors/suzuki-access-125-pearl-mirage-white-1486388664455.jpg?20190103151915&q=80",
                        height: 200,
                        width: 350,
                      ),
                      ElevatedButton(
                          onPressed: () {},
                          child: Text(
                            "ADD",
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.w600),
                          ))
                    ]),
                  ),
                  SizedBox(
                    width: 50,
                  ),
                  Container(
                    child: Column(children: [
                      Image.network(
                        "https://www.carandbike.com/_next/image?url=https%3A%2F%2Fimages.carandbike.com%2Fbike-images%2Fcolors%2Fsuzuki%2Fnew-access-125%2Fsuzuki-new-access-125-metallic-dark-greenish-blue-std.jpg%3Fv%3D1692875439&w=750&q=75",
                        height: 200,
                        width: 350,
                      ),
                      ElevatedButton(
                          onPressed: () {},
                          child: Text(
                            "ADD",
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.w600),
                          ))
                    ]),
                  ),
                ],
              ),
            ),
            Text(
              "Ktm",
              style: TextStyle(
                  color: Colors.black,
                  fontStyle: FontStyle.italic,
                  //   fontWeight: FontWeight.bold,
                  fontSize: 25,
                  shadows: [Shadow(color: Colors.black, offset: Offset(2, 1))]),
            ),
            SizedBox(
              width: 79,
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: [
                  SizedBox(
                    width: 50,
                  ),
                  Container(
                    child: Column(children: [
                      Image.network(
                        "https://imgd.aeplcdn.com/1280x720/n/cw/ec/49496/ktm-duke-200-side0.jpeg?q=100",
                        height: 200,
                        width: 300,
                      ),
                      ElevatedButton(
                          onPressed: () {},
                          child: Text(
                            "ADD",
                            style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.w600,
                            ),
                          ))
                    ]),
                  ),
                  SizedBox(
                    width: 70,
                  ),
                  Container(
                    child: Column(children: [
                      Image.network(
                        "https://imgd.aeplcdn.com/476x268/n/cw/ec/129741/125-duke-right-side-view.gif?isig=0",
                        height: 200,
                        width: 300,
                      ),
                      ElevatedButton(
                          onPressed: () {},
                          child: Text(
                            "ADD",
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.w600),
                          ))
                    ]),
                  ),
                  SizedBox(
                    width: 50,
                  ),
                  Container(
                    child: Column(children: [
                      Image.network(
                        "https://autocdn.co.uk/cdn-cgi/imagedelivery/JC4X6oe6GKVO4ZI4xd1Czg/1bfe34bf-2bae-4e66-4bee-f2996fe94d00/raw",
                        height: 200,
                        width: 330,
                      ),
                      ElevatedButton(
                          onPressed: () {},
                          child: Text(
                            "ADD",
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.w600),
                          ))
                    ]),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FloatingActionButton(
        backgroundColor: Color.fromARGB(255, 221, 153, 52),
        onPressed: () {},
        child: Icon(
          Icons.refresh,
          color: Colors.black,
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _selectedIndex,
        onTap: _onItemTapped,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.car_rental),
            label: 'Car',
          ),
        ],
      ),
    );
  }
}
