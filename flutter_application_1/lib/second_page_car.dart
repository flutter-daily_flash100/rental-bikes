import 'package:flutter/material.dart';
import 'package:flutter_application_1/bike.dart';

class CarScreen extends StatelessWidget {
  const CarScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue,
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Hyundai",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Image.network(
                  "https://apollo.olx.in/v1/files/4isgxm3popxy1-IN/image;s=360x0"),
            )
          ],
        ),
      ),
    );
  }
}
//